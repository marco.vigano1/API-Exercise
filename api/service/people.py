import redis
from redisdb import RedisDb
import json
from flask import request, Blueprint
import os
from config import config

# TODO bit of an hack with the config - need to improve this
env = os.getenv('APP_CONFIG') or 'default'
REDIS_HOST = config[env].REDIS_HOST
REDIS_PORT = config[env].REDIS_PORT

people = Blueprint('people', __name__)

# initialize database
db = RedisDb(redis.Redis(host=REDIS_HOST, port=REDIS_PORT,
                         charset="utf-8", decode_responses=True), 'person')

# headers are the same for each endpoint
headers = {'content-type': 'application/json'}


@people.route('/env', methods=['GET'])
def get_env():
    return json.dumps({'env': env, 'host_redis': REDIS_HOST})


@people.route('/people', methods=['GET'])
def get_all():
    people_list = db.get_all()
    return json.dumps(people_list), headers


@people.route('/people/<id>', methods=['GET'])
def get_by_id(id):
    person = db.get(id)
    if not person:
        return '', 404, headers
    return json.dumps(person), 200, headers


@people.route('/people', methods=['POST'])
def post():
    person = request.get_json()
    res, person_created = db.insert(person)
    if res:
        return json.dumps(person_created), 201, headers
    else:
        return '', 500, headers


@people.route('/people/<id>', methods=['DELETE'])
def delete(id):
    person_deleted = db.delete(id)
    if not person_deleted:
        return '', 404, headers
    return '', 200, headers


@people.route('/people/<id>', methods=['PUT'])
def put(id):
    person = request.get_json()
    person_updated = db.update(person, id)
    if not person_updated:
        return '', 404, headers
    return json.dumps(person_updated), 200, headers
