import uuid


class RedisDb(object):

    def __init__(self, redis, prefix):

        self._redis = redis
        self._prefix = prefix

    def get_all(self):
        ids = self._redis.keys('*' + self._prefix + '*')
        return [self._redis.hgetall(id) for id in ids]

    def get(self, id):
        return self._redis.hgetall(self._add_pref(id))

    def insert(self, data, id=None):
        if not id:
            id = str(uuid.uuid4())
        data['uuid'] = id
        id_with_prefix = self._add_pref(id)
        res = self._redis.hmset(id_with_prefix, data)
        return res, data

    def delete(self, id):
        id = self._add_pref(id)
        if not self._redis.exists(id):
            return False
        self._redis.delete(id)
        return True

    def update(self, data, id):
        deleted = self.delete(id)
        if deleted:
            return self.insert(data, id)
        return False

    def _add_pref(self, id):
        return self._prefix + ':' + id
