import unittest
import app
import json


class FlaskClientTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.create_app('test')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()

    def _post_payload(self):
        # post test payload
        payload = json.dumps({
                    "survived": 1,
                    "passengerClass": 1,
                    "name": "test",
                    "sex": "female",
                    "age": 41,
                    "siblingsOrSpousesAboard": 1,
                    "parentsOrChildrenAboard": 0,
                    "fare": 51.8625})

        resp_get = self.client.post('/people', data=payload, content_type='application/json')
        self.assertEqual(201, resp_get.status_code)
        return resp_get.json['uuid']

    def test_list_all(self):
        self._post_payload()

        response = self.client.get('/people')
        self.assertEqual(200, response.status_code)
        people = response.json
        self.assertTrue(len(people) > 0)

    def test_get(self):
        uuid = self._post_payload()

        # test get
        resp_get = self.client.get('/people/{}'.format(uuid))
        self.assertEqual(200, resp_get.status_code)

        # delete
        res_delete = self.client.delete('/people/{}'.format(uuid))
        self.assertEqual(200, res_delete.status_code)

    def test_post(self):
        uuid = self._post_payload()

        # check if resource created
        resp_get = self.client.get('/people/{}'.format(uuid))
        self.assertEqual(200, resp_get.status_code)

    def test_delete(self):
        uuid = self._post_payload()

        # delete
        res_delete = self.client.delete('/people/{}'.format(uuid))
        self.assertEqual(200, res_delete.status_code)

        # heck if resource created
        resp_get = self.client.get('/people/{}'.format(uuid))
        self.assertEqual(404, resp_get.status_code)
