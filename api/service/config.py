import os


class Config(object):
    @staticmethod
    def init_app(app):
        pass


class DefaultConfig(Config):
    REDIS_HOST = os.environ.get('REDIS_HOST') or 'localhost'
    REDIS_PORT = os.environ.get('REDIS_PORT') or 6379


class TestConfig(Config):
    REDIS_HOST = os.environ.get('REDIS_HOST') or 'localhost'
    REDIS_PORT = os.environ.get('REDIS_PORT') or 6379


class DevConfig(Config):
    REDIS_HOST = os.environ.get('REDIS_HOST') or 'redis'
    REDIS_PORT = os.environ.get('REDIS_PORT', 6379)


config = {
    'default': DefaultConfig,
    'test': TestConfig,
    'dev': DevConfig
}
