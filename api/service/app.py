from flask import Flask
import people as people
from config import config
import os


def create_app(config_name):
    app = Flask(__name__)

    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    app.register_blueprint(people.people)

    return app


if __name__ == '__main__':
    app = create_app(os.getenv('APP_CONFIG') or 'default')
    app.run(host='0.0.0.0')
