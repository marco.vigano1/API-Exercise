#!/bin/bash

# Loading data and Dumping it in data folder
rm -f data/dump.rdb
docker build -f Dockerfile.prep . -t redis-tmp
docker run --name redis-tmp -v $PWD/data:/data -d redis-tmp
docker exec -ti redis-tmp sh -c "cat titanic.txt | redis-cli --pipe"
docker exec -ti redis-tmp sh -c "echo "SAVE" | redis-cli"
docker exec -ti redis-tmp sh -c "cp dump.rdb /data/"
docker stop redis-tmp

# Final Image with data
docker build -f Dockerfile.final . -t redis-data:v1