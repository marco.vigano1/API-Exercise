import csv
import uuid
import argparse
import os

# parse arguments
parser = argparse.ArgumentParser(description='Arguments')
parser.add_argument('--input', type=str, default="titanic.csv", help='an input file name ')
parser.add_argument('--output', type=str, default="titanic.txt", help='an input file name ')
args = parser.parse_args()


def convert_csv_to_json(line):
    person = {
        'uuid': str(uuid.uuid4()),
        'survived': line['Survived'],
        'passengerClass': line['Pclass'],
        'name': line['Name'],
        'sex': line['Sex'],
        'age': line['Age'],
        'siblingsOrSpousesAboard': line['Siblings/Spouses Aboard'],
        'parentsOrChildrenAboard': line['Parents/Children Aboard'],
        'fare': line['Fare']
    }
    return person


def to_redis_protocol(person):

    uuid = person.get('uuid')
    prefix_uuid = 'person:'+person.get('uuid')
    survived = person.get('survived')
    passenger_class = person.get('passengerClass')
    name = person.get('name')
    sex = person.get('sex')
    age = person.get('age')
    siblings_or_spouses_aboard = person.get('siblingsOrSpousesAboard')
    parents_or_children_aboard = person.get('parentsOrChildrenAboard')
    fare = person.get('fare')

    return '*20\r\n$5\r\nHMSET' \
           '\r\n${}\r\n{}' \
           '\r\n$4\r\nuuid\r\n${}\r\n{}' \
           '\r\n$8\r\nsurvived\r\n${}\r\n{}' \
           '\r\n$14\r\npassengerClass\r\n${}\r\n{}' \
           '\r\n$4\r\nname\r\n${}\r\n{}' \
           '\r\n$3\r\nsex\r\n${}\r\n{}' \
           '\r\n$3\r\nage\r\n${}\r\n{}' \
           '\r\n$23\r\nsiblingsOrSpousesAboard\r\n${}\r\n{}' \
           '\r\n$23\r\nparentsOrChildrenAboard\r\n${}\r\n{}' \
           '\r\n$4\r\nfare\r\n${}\r\n{}\r\n'.format(
                len(prefix_uuid), prefix_uuid,
                len(uuid), uuid,
                len(survived), survived,
                len(passenger_class), passenger_class,
                len(name), name,
                len(sex), sex,
                len(age), age,
                len(siblings_or_spouses_aboard), siblings_or_spouses_aboard,
                len(parents_or_children_aboard), parents_or_children_aboard,
                len(fare), fare)


if __name__ == "__main__":

    dir = os.path.dirname(__file__)
    input_file = os.path.join(dir, args.input)
    output_file = os.path.join(dir, args.output)

    with open(input_file) as input_f, open(output_file, 'w') as output_f:
        reader = csv.DictReader(input_f)
        for row in reader:
            person = convert_csv_to_json(row)
            person_to_redis = to_redis_protocol(person)
            output_f.write(person_to_redis)

    input_f.close()
    output_f.close()
